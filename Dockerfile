FROM alpine:3 as builder

WORKDIR /tmp/insightful-sqs

RUN \
  apk add --no-cache git curl jq \
  && git clone --verbose --depth=1 https://github.com/kobim/sqs-insight.git \
  && export elasticmq_version=$(curl -sL https://api.github.com/repos/adamw/elasticmq/releases/latest | jq -r .tag_name) \
  && elasticmq_version=${elasticmq_version//v} \
  && curl -LO https://s3-eu-west-1.amazonaws.com/softwaremill-public/elasticmq-server-${elasticmq_version}.jar \
  && mkdir -p elasticmq \
  && mv elasticmq-server-${elasticmq_version}.jar elasticmq/elasticmq-server.jar

FROM eclipse-temurin:22-jre-alpine

COPY --from=builder /tmp/insightful-sqs/ /opt/
COPY etc/ /etc/
COPY config/sqs-insight.conf /opt/sqs-insight/config/config_local.json
COPY config/elasticmq.conf /opt/elasticmq/config/elasticmq.conf

RUN \
  apk add --no-cache nodejs npm supervisor \
  && rm -rf /etc/supervisord.conf \
  && ln -s /etc/supervisor/supervisord.conf /etc/supervisord.conf \
  && cd /opt/sqs-insight \
  && npm install

EXPOSE 9324 9325 9326

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
