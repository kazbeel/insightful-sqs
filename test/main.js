const {
  SQSClient,
  CreateQueueCommand,
  SendMessageCommand,
  ReceiveMessageCommand,
} = require("@aws-sdk/client-sqs");

// Create an SQS service client object
const sqsClient = new SQSClient({
  endpoint: process.env.AWS_SQS_ENDPOINT,
});

async function main() {
  try {
    // Create a new SQS queue
    const createQueueResult = await sqsClient.send(
      new CreateQueueCommand({ QueueName: "TestQueue" }),
    );
    console.log(
      "Successfully created new SQS queue with URL",
      createQueueResult.QueueUrl,
    );

    // Send a message to the queue
    const sendMessageResult = await sqsClient.send(
      new SendMessageCommand({
        QueueUrl: createQueueResult.QueueUrl,
        MessageBody: "Hello, world!",
      }),
    );
    console.log(
      "Successfully sent message with ID",
      sendMessageResult.MessageId,
    );

    // Receive a message from the queue
    const receiveMessageResult = await sqsClient.send(
      new ReceiveMessageCommand({
        QueueUrl: createQueueResult.QueueUrl,
        MaxNumberOfMessages: 1,
      }),
    );

    if (receiveMessageResult.Messages) {
      console.log("Received message:", receiveMessageResult.Messages[0].Body);
    } else {
      console.log("No messages received");
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}

main();
